package com.sip.domain;
import org.springframework.roo.addon.dbre.RooDbManaged;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooDbManaged(automaticallyDelete = true)
@RooToString(excludeFields = { "refMetodologis", "luaranPenelitians", "peranPenelitis", "progressPenelitians", "resumePenelitians", "idBidangKajian", "idSkema" })
@RooJpaActiveRecord(versionField = "", table = "penelitian", finders = { "findPenelitiansByTahunAnggaran" })
public class Penelitian {
}
