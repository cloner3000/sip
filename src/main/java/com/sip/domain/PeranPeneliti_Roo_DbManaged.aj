// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.sip.domain;

import com.sip.domain.MasterPeneliti;
import com.sip.domain.Penelitian;
import com.sip.domain.PeranPeneliti;
import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

privileged aspect PeranPeneliti_Roo_DbManaged {
    
    @ManyToOne
    @JoinColumn(name = "ID_PENELITIAN", referencedColumnName = "ID_PENELITIAN", nullable = false, insertable = false, updatable = false)
    private Penelitian PeranPeneliti.idPenelitian;
    
    @ManyToOne
    @JoinColumn(name = "ID_PENELITI", referencedColumnName = "ID_PENELITI", nullable = false, insertable = false, updatable = false)
    private MasterPeneliti PeranPeneliti.idPeneliti;
    
    @Column(name = "POSISI")
    @NotNull
    private boolean PeranPeneliti.posisi;
    
    public Penelitian PeranPeneliti.getIdPenelitian() {
        return idPenelitian;
    }
    
    public void PeranPeneliti.setIdPenelitian(Penelitian idPenelitian) {
        this.idPenelitian = idPenelitian;
    }
    
    public MasterPeneliti PeranPeneliti.getIdPeneliti() {
        return idPeneliti;
    }
    
    public void PeranPeneliti.setIdPeneliti(MasterPeneliti idPeneliti) {
        this.idPeneliti = idPeneliti;
    }
    
    public boolean PeranPeneliti.isPosisi() {
        return posisi;
    }
    
    public void PeranPeneliti.setPosisi(boolean posisi) {
        this.posisi = posisi;
    }
    
}
