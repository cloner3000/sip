// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.sip.domain;

import com.sip.domain.ProgressPenelitian;
import com.sip.domain.RefProgress;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

privileged aspect RefProgress_Roo_DbManaged {
    
    @OneToMany(mappedBy = "idProgress")
    private Set<ProgressPenelitian> RefProgress.progressPenelitians;
    
    @Column(name = "TAHAP", length = 250)
    @NotNull
    private String RefProgress.tahap;
    
    @Column(name = "KETERANGAN", length = 300)
    private String RefProgress.keterangan;
    
    public Set<ProgressPenelitian> RefProgress.getProgressPenelitians() {
        return progressPenelitians;
    }
    
    public void RefProgress.setProgressPenelitians(Set<ProgressPenelitian> progressPenelitians) {
        this.progressPenelitians = progressPenelitians;
    }
    
    public String RefProgress.getTahap() {
        return tahap;
    }
    
    public void RefProgress.setTahap(String tahap) {
        this.tahap = tahap;
    }
    
    public String RefProgress.getKeterangan() {
        return keterangan;
    }
    
    public void RefProgress.setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }
    
}
