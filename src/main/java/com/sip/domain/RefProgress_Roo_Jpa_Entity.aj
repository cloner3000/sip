// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.sip.domain;

import com.sip.domain.RefProgress;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

privileged aspect RefProgress_Roo_Jpa_Entity {
    
    declare @type: RefProgress: @Entity;
    
    declare @type: RefProgress: @Table(name = "ref_progress");
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID_PROGRESS")
    private Integer RefProgress.idProgress;
    
    public Integer RefProgress.getIdProgress() {
        return this.idProgress;
    }
    
    public void RefProgress.setIdProgress(Integer id) {
        this.idProgress = id;
    }
    
}
