// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.sip.domain;

import com.mysema.query.BooleanBuilder;
import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.path.PathBuilder;
import com.sip.domain.RefSkemaPenelitian;
import com.sip.domain.RefSkemaPenelitianBatchService;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.springframework.transaction.annotation.Transactional;

privileged aspect RefSkemaPenelitianBatchService_Roo_GvNIXJpaBatch {
    
    public Class RefSkemaPenelitianBatchService.getEntity() {
        return RefSkemaPenelitian.class;
    }
    
    public EntityManager RefSkemaPenelitianBatchService.entityManager() {
        return RefSkemaPenelitian.entityManager();
    }
    
    @Transactional
    public int RefSkemaPenelitianBatchService.deleteAll() {
        return entityManager().createQuery("DELETE FROM RefSkemaPenelitian").executeUpdate();
    }
    
    @Transactional
    public int RefSkemaPenelitianBatchService.deleteIn(List<Integer> ids) {
        Query query = entityManager().createQuery("DELETE FROM RefSkemaPenelitian as r WHERE r.idSkema IN (:idList)");
        query.setParameter("idList", ids);
        return query.executeUpdate();
    }
    
    @Transactional
    public int RefSkemaPenelitianBatchService.deleteNotIn(List<Integer> ids) {
        Query query = entityManager().createQuery("DELETE FROM RefSkemaPenelitian as r WHERE r.idSkema NOT IN (:idList)");
        query.setParameter("idList", ids);
        return query.executeUpdate();
    }
    
    @Transactional
    public void RefSkemaPenelitianBatchService.create(List<RefSkemaPenelitian> refSkemaPenelitians) {
        for( RefSkemaPenelitian refskemapenelitian : refSkemaPenelitians) {
            refskemapenelitian.persist();
        }
    }
    
    @Transactional
    public List<RefSkemaPenelitian> RefSkemaPenelitianBatchService.update(List<RefSkemaPenelitian> refskemapenelitians) {
        List<RefSkemaPenelitian> merged = new ArrayList<RefSkemaPenelitian>();
        for( RefSkemaPenelitian refskemapenelitian : refskemapenelitians) {
            merged.add( refskemapenelitian.merge() );
        }
        return merged;
    }
    
    public List<RefSkemaPenelitian> RefSkemaPenelitianBatchService.findByValues(Map<String, Object> propertyValues) {
        
        // if there is a filter
        if (propertyValues != null && !propertyValues.isEmpty()) {
            // Prepare a predicate
            BooleanBuilder baseFilterPredicate = new BooleanBuilder();
            
            // Base filter. Using BooleanBuilder, a cascading builder for
            // Predicate expressions
            PathBuilder<RefSkemaPenelitian> entity = new PathBuilder<RefSkemaPenelitian>(RefSkemaPenelitian.class, "entity");
            
            // Build base filter
            for (String key : propertyValues.keySet()) {
                baseFilterPredicate.and(entity.get(key).eq(propertyValues.get(key)));
            }
            
            // Create a query with filter
            JPAQuery query = new JPAQuery(RefSkemaPenelitian.entityManager());
            query = query.from(entity);
            
            // execute query
            return query.where(baseFilterPredicate).list(entity);
        }
        
        // no filter: return all elements
        return RefSkemaPenelitian.findAllRefSkemaPenelitians();
    }
    
    @Transactional
    public long RefSkemaPenelitianBatchService.deleteByValues(Map<String, Object> propertyValues) {
        
        // if there no is a filter
        if (propertyValues == null || propertyValues.isEmpty()) {
            throw new IllegalArgumentException("Missing property values");
        }
        // Prepare a predicate
        BooleanBuilder baseFilterPredicate = new BooleanBuilder();
        
        // Base filter. Using BooleanBuilder, a cascading builder for
        // Predicate expressions
        PathBuilder<RefSkemaPenelitian> entity = new PathBuilder<RefSkemaPenelitian>(RefSkemaPenelitian.class, "entity");
        
        // Build base filter
        for (String key : propertyValues.keySet()) {
            baseFilterPredicate.and(entity.get(key).eq(propertyValues.get(key)));
        }
        
        // Create a query with filter
        JPADeleteClause delete = new JPADeleteClause(RefSkemaPenelitian.entityManager(),entity);
        
        // execute delete
        return delete.where(baseFilterPredicate).execute();
    }
    
    @Transactional
    public void RefSkemaPenelitianBatchService.delete(List<RefSkemaPenelitian> refskemapenelitians) {
        for( RefSkemaPenelitian refskemapenelitian : refskemapenelitians) {
            refskemapenelitian.remove();
        }
    }
    
}
