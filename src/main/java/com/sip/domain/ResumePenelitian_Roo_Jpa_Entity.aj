// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.sip.domain;

import com.sip.domain.ResumePenelitian;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

privileged aspect ResumePenelitian_Roo_Jpa_Entity {
    
    declare @type: ResumePenelitian: @Entity;
    
    declare @type: ResumePenelitian: @Table(name = "resume_penelitian");
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID_ABSTRAKSI")
    private Integer ResumePenelitian.idAbstraksi;
    
    public Integer ResumePenelitian.getIdAbstraksi() {
        return this.idAbstraksi;
    }
    
    public void ResumePenelitian.setIdAbstraksi(Integer id) {
        this.idAbstraksi = id;
    }
    
}
