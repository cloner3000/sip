package com.sip.web;
import com.sip.domain.MasterPeneliti;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.gvnix.addon.web.mvc.annotations.jquery.GvNIXWebJQuery;
import com.sip.domain.MasterPenelitiBatchService;
import org.gvnix.addon.web.mvc.annotations.batch.GvNIXWebJpaBatch;
import org.gvnix.addon.datatables.annotations.GvNIXDatatables;

@RequestMapping("/masterpenelitis")
@Controller
@RooWebScaffold(path = "masterpenelitis", formBackingObject = MasterPeneliti.class)
@GvNIXWebJQuery
@GvNIXWebJpaBatch(service = MasterPenelitiBatchService.class)
@GvNIXDatatables(ajax = true)
public class MasterPenelitiController {
}
