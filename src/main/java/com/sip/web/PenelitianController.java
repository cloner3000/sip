package com.sip.web;
import com.sip.domain.Penelitian;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.roo.addon.web.mvc.controller.finder.RooWebFinder;
import org.gvnix.addon.web.mvc.annotations.jquery.GvNIXWebJQuery;
import com.sip.domain.PenelitianBatchService;
import org.gvnix.addon.web.mvc.annotations.batch.GvNIXWebJpaBatch;
import org.gvnix.addon.datatables.annotations.GvNIXDatatables;

@RequestMapping("/penelitians")
@Controller
@RooWebScaffold(path = "penelitians", formBackingObject = Penelitian.class)
@RooWebFinder
@GvNIXWebJQuery
@GvNIXWebJpaBatch(service = PenelitianBatchService.class)
@GvNIXDatatables(ajax = true)
public class PenelitianController {
}
