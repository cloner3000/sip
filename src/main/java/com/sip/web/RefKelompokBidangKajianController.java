package com.sip.web;
import com.sip.domain.RefKelompokBidangKajian;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.gvnix.addon.web.mvc.annotations.jquery.GvNIXWebJQuery;
import com.sip.domain.RefKelompokBidangKajianBatchService;
import org.gvnix.addon.web.mvc.annotations.batch.GvNIXWebJpaBatch;
import org.gvnix.addon.datatables.annotations.GvNIXDatatables;

@RequestMapping("/refkelompokbidangkajians")
@Controller
@RooWebScaffold(path = "refkelompokbidangkajians", formBackingObject = RefKelompokBidangKajian.class)
@GvNIXWebJQuery
@GvNIXWebJpaBatch(service = RefKelompokBidangKajianBatchService.class)
@GvNIXDatatables(ajax = true)
public class RefKelompokBidangKajianController {
}
