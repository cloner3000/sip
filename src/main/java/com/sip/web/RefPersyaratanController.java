package com.sip.web;
import com.sip.domain.RefPersyaratan;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.gvnix.addon.web.mvc.annotations.jquery.GvNIXWebJQuery;
import com.sip.domain.RefPersyaratanBatchService;
import org.gvnix.addon.web.mvc.annotations.batch.GvNIXWebJpaBatch;
import org.gvnix.addon.datatables.annotations.GvNIXDatatables;

@RequestMapping("/refpersyaratans")
@Controller
@RooWebScaffold(path = "refpersyaratans", formBackingObject = RefPersyaratan.class)
@GvNIXWebJQuery
@GvNIXWebJpaBatch(service = RefPersyaratanBatchService.class)
@GvNIXDatatables(ajax = true)
public class RefPersyaratanController {
}
