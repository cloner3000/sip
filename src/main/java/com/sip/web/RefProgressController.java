package com.sip.web;
import com.sip.domain.RefProgress;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.gvnix.addon.web.mvc.annotations.jquery.GvNIXWebJQuery;
import com.sip.domain.RefProgressBatchService;
import org.gvnix.addon.web.mvc.annotations.batch.GvNIXWebJpaBatch;
import org.gvnix.addon.datatables.annotations.GvNIXDatatables;

@RequestMapping("/refprogresses")
@Controller
@RooWebScaffold(path = "refprogresses", formBackingObject = RefProgress.class)
@GvNIXWebJQuery
@GvNIXWebJpaBatch(service = RefProgressBatchService.class)
@GvNIXDatatables(ajax = true)
public class RefProgressController {
}
