package com.sip.web;
import com.sip.domain.RefSkemaPenelitian;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.gvnix.addon.web.mvc.annotations.jquery.GvNIXWebJQuery;
import com.sip.domain.RefSkemaPenelitianBatchService;
import org.gvnix.addon.web.mvc.annotations.batch.GvNIXWebJpaBatch;
import org.gvnix.addon.datatables.annotations.GvNIXDatatables;

@RequestMapping("/refskemapenelitians")
@Controller
@RooWebScaffold(path = "refskemapenelitians", formBackingObject = RefSkemaPenelitian.class)
@GvNIXWebJQuery
@GvNIXWebJpaBatch(service = RefSkemaPenelitianBatchService.class)
@GvNIXDatatables(ajax = true)
public class RefSkemaPenelitianController {
}
